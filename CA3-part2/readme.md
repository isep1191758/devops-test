Duarte Rothes Barbosa
1191758@isep.ipp.pt

# Class Assignment 3, Part2 - Turma B, Grupo 5

29-04-2020

Objetivos/Requisitos
--------------------

   Objetivo Principal:

   Usar a ferramenta Oracle VM VirtualBox para efetuar as mesmas operações de exercícios anteriores (Spring Boot Tutorial Rest) + Construção DB em H2, através da instalação do software Vagrant que executa a gestão de duas VM do tipo web e db.

   Requisitos:

   1. Instalar o programa Vagrant através de https://www.vagrantup.com/downloads.html;


   2. Colocar o vagrantfile gerado na pasta de trabalho respeitante a este Assignment dentro do meu repositório, no meu caso CA3-part2;


   3. O vagrantfile é criado através do comando 'vagrant init envimation/ubuntu-xenial';


   4. Na continuação, e para projetos web, aceder ao browser do Host;


   5. Descrever o processo tomado num ficheiro readme.md


   6. Efetuar uma tag com a designação 'CA3-part2'




# A. Início


O primeiro passo é criar uma pasta nova para este Assignement com o nome CA3-part2, localizada na raiz, e efetuar um commit através de:


    ->   git add CA3-part2
    ->   git commit -m "CA3-part2 folder creation"
    ->   git push




# B. Reunião de condições para arranque


  1. Primeiramente, fazer o download do Vagrantfile através de https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/src/master/, e colocá-lo na pasta criada para o efeito, substituindo o Vagranfile originado pela instalação do programa Vagrant;
  

  2. No mesmo ficheiro, efetuar a adaptação do repositório na linha 70 para:
  

        ->   git clone https://isep1191758@bitbucket.org/isep1191758/devops-19-20-b-1191758.git
        ->   cd CA2-part2
        ->   ./gradlew clean build

     e seguidamente, gravar as alterações no ficheiro;
     

  3. Deste modo, o Vagrant já está direcionado para a minha versão de Spring Application, resultante do Assignment CA2-part2 no meu repositório.





# C. Alterações na minha Versão SPRING BOOT APPLICATION


  1. Abrir no browser o endereço "https://bitbucket.org/atb/tut-basic-gradle/src/master/"
  

  2. Replicar as alterações constantes no mesmo, na minha versão, e efetuar os mesmos commits descritos;
  

  3. Deste modo, a minha versão ficará adaptada para "correr" as duas tarefas do Assignment no browser, através da linha de comandos Vagrant.
  




# D. Linha de Comandos


  1. Abrir uma linha de comando no Host no modo administrador;
  

  2. Direccionar a raiz para a localização da minha pasta dentro do repositório:
  

           ->   CA3-part2
           

  3. Verificar a boa instalação do Vagrant através de:
  

           ->   vagrant -v
           

     cuja resposta é Vagrant 2.2.7;
     

  4. Instalar/Acionar as duas máquinas virtuais DB e WEB através do comando "vagrant up";
  

  5. Após a instalação/arranque das duas VM, efetuar a instalação dos seguintes programas através dos comandos:
  

         ->   sudo apt install git
         ->   sudo apt install openjdk-8-jdk-headless
         ->   sudo apt install openssh-server
         

  6. Para me transferir para a VM web, executei o comando 'vagrant ssh web', e na raiz aparece "vagrant@web" a verde;
  

  7. Para a instalação do Gradle na sua versão mais recente (6.3), utilizei os seguintes comandos:
  

         ->   wget https://services.gradle.org/distributions/gradle-6.3-bin.zip -P /tmp (faz o download da versão)
         ->   sudo unzip -d /opt/gradle /tmp/gradle-6.3.zip (descomprime o ficheiro)
         ->   sudo vi /etc/profile.d/gradle.sh (cria o ficheiro gradle.sh para conter os PATH's das variáveis de ambiente)
         

     Conteúdo do ficheiro gradle.sh: - export GRADLE_HOME=/opt/gradle/gradle-6.3
                                     - export PATH=${GRADLE_HOME}/bin:${PATH}

         ->   source /etc/profile.d/gradle.sh (executa o ficheiro gradle.sh)
         

  8. A seguir, e depois de verificada a versão através de 'gradle -v', executo o comando:
  

         ->   gradlew clean build
         

  9. Finalmente, e no fim do procedimento, vou ao browser no endereço "http://localhost:8080/demo-0.0.1-SNAPSHOT/", e aparece a tabela preenchida com os atributos FirstName, LastName, Description e Email;
  

     O mesmo acontece quando digito no browser o endereço "http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console/", em que aparece a caixa de diálogo Login, Driver Class e JDBC URL (neste campo dever-se-á introduzir 'jdbc:h2:tcp://192.168.33.11:9092/./jpadb').
     

  10. Efetuar no IDE, os seguintes comandos na raiz do repositório (C:\Users\ASUS\IdeaProjects\devops-19-20-b-1191758\):
  

          ->   git tag CA3-part2
          ->   git push origin master CA3-part2.